require 'rails_helper'

RSpec.describe Trip, type: :model do

  it { should have_many(:locations).dependent(:destroy) }
  it { should validate_presence_of(:status)  }

  describe 'Update trip status after completed' do
    before { @trip = Trip.create(status: "completed") }
    it 'returns an error when update trip from :completed to :ongoing' do
      expect { @trip.update(status: "ongoing") }.to raise_error("Status trip status cannot changed after completed")
    end
  end
end
