FactoryGirl.define do
  factory :location do
    latitude { Faker::Address.latitude }
    longitude { Faker::Address.longitude }
    trip_id nil
  end
end
