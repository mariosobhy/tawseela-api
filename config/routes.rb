require 'sidekiq/web'


Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  mount Sidekiq::Web => '/sidekiq'

  # access subdomain API at 'api.dev.localhost:3000/trips' on your local host
  constraints subdomain: 'api' do
    namespace :api, path: '/' do
      resources :trips do
        resources :locations
      end
    end
  end
end
