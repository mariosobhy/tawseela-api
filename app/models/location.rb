class Location < ApplicationRecord
  #-------------------------------- Associations -----------------------------------
  belongs_to :trip, inverse_of: :locations, touch: true

  #-------------------------------- Validations -----------------------------------
  #  validate existing of latitude and longitude
  validates_presence_of :latitude, :longitude
end
