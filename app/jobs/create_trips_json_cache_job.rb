class CreateTripsJsonCacheJob < ApplicationJob
  queue_as :default

  def perform(*args)
    # Do something later
    trips = Trip.includes(:locations)
    Rails.cache.fetch(Trip.cache_key(trips)) do 
      trips.to_json(include: :locations)
    end 
  end
end
